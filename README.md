
PLB OAIH Repo for TP1
Will install 3 nodes HDP cluster

Requirements on 27 March 2021
- Vagrant: version 2.2.9
- Oracle VirtualBox: 6.1.16 or +

ambari-vagrant
==============

Vagrant setup for creating Ambari development/test virtual machines

Please see: https://cwiki.apache.org/confluence/display/AMBARI/Quick+Start+Guide

Go in folder centos6.8 and run: vagrant up
It will provision 3 VMs in Oracle VB
